//
//  ViewController.swift
//  hWorld
//
//  Created by erjigit on 1/10/20.
//  Copyright © 2020 erjigit. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloWorld: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helloWorld.isHidden = true
        startButton.layer.cornerRadius = 10
    }

    @IBAction func startButtonPressed() {
        if helloWorld.isHidden {
            helloWorld.isHidden = false
            startButton.setTitle("Clear Text", for: .normal)
        } else {
            helloWorld.isHidden = true
            startButton.setTitle("Show Text", for: .normal)
        }
    }
    
}

